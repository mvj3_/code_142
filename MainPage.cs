using System;
 using System.Windows;
 using Microsoft.Phone.Controls;
 using System.Windows.Media.Imaging;
 
 namespace ImageCacheDemo
 {
     public partial class MainPage : PhoneApplicationPage
     {
         public String ImageUrl1 = "http://i.s-microsoft.com/global/ImageStore/PublishingImages/FY13/Asset/features/Surface_BG_0114_1600x540_EN_US.jpg";
         public String ImageUrl2 = "http://i.s-microsoft.com/global/ImageStore/PublishingImages/FY13/Asset/features/WPBrand_BG_1112_1600x540_EN_US.jpg";
         // 构造函数
         public MainPage()
         {
             InitializeComponent();
             
         }
         protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
         {
             base.OnNavigatedTo(e);
             this.imagesStackPanel.DataContext = this.ImageUrl1;
         }
 
         private void Button_Click(object sender, RoutedEventArgs e)
         {
             BitmapImage bitmapImage = new BitmapImage();
             bitmapImage.DownloadProgress += bitmapImage_DownloadProgress;
             //使用方式2
             Cache.ImageCache.SetSource(bitmapImage,this.ImageUrl2);
             this.bitmapImageImage.Source = bitmapImage;
         }
 
         void bitmapImage_DownloadProgress(object sender, DownloadProgressEventArgs e)
         {
             this.imageDownloadProgress.Value = e.Progress;
         }
     }
 }